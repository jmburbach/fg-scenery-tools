#!/usr/bin/env python
# This file is part of fg-scenery-tools <https://gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2011 Jacob M. Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
from fgbucket import Bucket
import os, glob, math, sys, ogr, osr


def tile_root(lat, lon):
	base_lat = int(abs(math.floor(lat / 10.0) * 10.0))
	base_lon = int(abs(math.floor(lon / 10.0) * 10.0))
	lat_prefix = lat < 0 and 's' or 'n'
	lon_prefix = lon < 0 and 'w' or 'e'
	root = "%s%03d%s%02d" % (lon_prefix, base_lon, lat_prefix, base_lat)
	return root

def tile_name(lat, lon):
	lat_prefix = lat < 0 and 's' or 'n'
	lon_prefix = lon < 0 and 'w' or 'e'
	name = "%s%03d%s%02d" % (lon_prefix, abs(lon), lat_prefix, abs(lat))
	return name

def tile_path(lat, lon):
	return os.path.join(tile_root(lat, lon), tile_name(lat, lon))

def main():
	if len(sys.argv) < 4:
		print "usage: %s infile outfile scenery_path" % os.path.basename(sys.argv[0])
		print "    infile: Path to file containing tile coordinates. Should be in format of"
		print "            one comma separated lat,lon value per line."
		print 
		print "    outfile: Path for the output shapefile...will be overwritten if it exists."
		print
		print "    scenery_path: Path to a top level flightgear scenery installation. Script"
		print "                  will look in scenery_path/Terrain for the tiles to process."
		return
	
	infile = sys.argv[1]
	if not os.path.exists(infile) or not os.path.isfile(infile):
		print "%s does not exist or is not a regular file" % infile
		return

	outfile = sys.argv[2]
	if os.path.exists(outfile):
		print "warning: %s exists and will be overwritten" % outfile
		
	scenery_path = sys.argv[3]
	if not os.path.exists(scenery_path) or not os.path.isdir(scenery_path):
		print "%s does not exist or is not a directory" % scenery_path
		return

	driver = ogr.GetDriverByName("ESRI Shapefile")
	
	spatial = osr.SpatialReference()
	spatial.ImportFromEPSG(4326)
	spatial.MorphToESRI()
	
	basename = os.path.splitext(outfile)[0]

	shp_fname = basename + ".shp"		
	if os.path.exists(shp_fname):
		driver.DeleteDataSource(shp_fname)
	ds = driver.CreateDataSource(shp_fname)
	assert ds is not None

	prj_fname = basename + ".prj"
	file(prj_fname, "w").write(spatial.ExportToWkt())
	
	layer = ds.CreateLayer(basename, geom_type = ogr.wkbPolygon)

	label_field = ogr.FieldDefn("label", ogr.OFTString)
	label_field.SetWidth(16)
	bucket_field = ogr.FieldDefn("bucket", ogr.OFTInteger)
	bucket_field.SetWidth(7)
	min_lat_field = ogr.FieldDefn("min_lat", ogr.OFTReal)
	max_lat_field = ogr.FieldDefn("max_lat", ogr.OFTReal)
	min_lon_field = ogr.FieldDefn("min_lon", ogr.OFTReal)
	max_lon_field = ogr.FieldDefn("max_lon", ogr.OFTReal)
	center_lat_field = ogr.FieldDefn("center_lat", ogr.OFTReal)
	center_lon_field = ogr.FieldDefn("center_lon", ogr.OFTReal)
	width_field = ogr.FieldDefn("width", ogr.OFTReal)
	height_field = ogr.FieldDefn("height", ogr.OFTReal)
	grid_x_field = ogr.FieldDefn("grid_x", ogr.OFTReal)
	grid_y_field = ogr.FieldDefn("grid_y", ogr.OFTReal)
	tile_field = ogr.FieldDefn("tile", ogr.OFTString)
	tile_field.SetWidth(7)
	path_field = ogr.FieldDefn("path", ogr.OFTString)
	path_field.SetWidth(23)

	layer.CreateField(label_field)
	layer.CreateField(bucket_field)
	layer.CreateField(min_lat_field)
	layer.CreateField(max_lat_field)
	layer.CreateField(min_lon_field)
	layer.CreateField(max_lon_field)
	layer.CreateField(center_lat_field)
	layer.CreateField(center_lon_field)
	layer.CreateField(width_field)
	layer.CreateField(height_field)
	layer.CreateField(grid_x_field)
	layer.CreateField(grid_y_field)
	layer.CreateField(tile_field)
	layer.CreateField(path_field)

	feature_def = layer.GetLayerDefn()

	fin = file(infile, "r")
	for line in fin:
		line = line.strip()
		if not line or line[0] == "#":
			continue
		lat, lon = map(int, line.split(","))
		path = tile_path(lat, lon)
		name = tile_name(lat, lon)
		
		stg_list = glob.glob(os.path.join(scenery_path, "Terrain", path, "*.stg"))
		for stg in stg_list:
			index = int(os.path.basename(stg).split(".")[0])

			b = Bucket(index)

			ring = ogr.Geometry(ogr.wkbLinearRing)
			ring.AddPoint(b.min_lon, b.min_lat)
			ring.AddPoint(b.max_lon, b.min_lat)
			ring.AddPoint(b.max_lon, b.max_lat)
			ring.AddPoint(b.min_lon, b.max_lat)
			ring.AddPoint(b.min_lon, b.min_lat)

			poly = ogr.Geometry(ogr.wkbPolygon)
			poly.AddGeometry(ring)

			feature = ogr.Feature(feature_def)
			feature.SetGeometry(poly)
			feature.SetField("label", name + "\n\n" + str(index))
			feature.SetField("tile", name)
			feature.SetField("path", os.path.join(path, str(index)))
			feature.SetField("bucket", index)
			feature.SetField("min_lat", b.min_lat)
			feature.SetField("max_lat", b.max_lat)
			feature.SetField("min_lon", b.min_lon)
			feature.SetField("max_lon", b.max_lon)
			feature.SetField("center_lat", b.center_lat)
			feature.SetField("center_lon", b.center_lon)
			feature.SetField("width", b.width)
			feature.SetField("height", b.height)
			feature.SetField("grid_x", b.x)
			feature.SetField("grid_y", b.y)

			layer.CreateFeature(feature)

			ring.Destroy()
			poly.Destroy()
			feature.Destroy()

	ds.Destroy()


if __name__ == "__main__":
	main()
