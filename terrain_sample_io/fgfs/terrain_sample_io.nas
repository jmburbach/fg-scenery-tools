# This file is part of fg-scenery-tools <gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2010 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
var NORTH = 0.0;
var EAST = 90.0;
var SOUTH = 180.0;
var WEST = 270.0;

var fprintf = func(handle, format_and_vargs...)
{
	io.write(handle, call(sprintf, format_and_vargs));
}

var distance_lat_m = func(min_coord, max_coord)
{
	var tmp = geo.Coord.new(min_coord);
	tmp.set_lon(max_coord.lon());
	return tmp.distance_to(max_coord);
}

var distance_lon_m = func(min_coord, max_coord)
{
	var tmp = geo.Coord.new(min_coord);
	tmp.set_lat(max_coord.lat());
	return tmp.distance_to(max_coord);
}

var export_region = func(min_lat, min_lon, max_lat, max_lon, resolution, filename, overwrite = 0)
{
	var span_lat = max_lat - min_lat;
    var span_lon = max_lon - min_lon;

	var median_lat = min_lat + (span_lat * 0.5);
	var median_lon = min_lon + (span_lon * 0.5);
	var median_alt = geo.elevation(median_lat, median_lon, 10000);

	var min_coord = geo.Coord.new();
	min_coord.set_latlon(min_lat, min_lon, geo.elevation(min_lat, min_lon, 10000));

	var max_coord = geo.Coord.new();
	max_coord.set_latlon(max_lat, max_lon, geo.elevation(max_lat, max_lon, 10000));
	
	# get longitudal distance in meters across median
	var median_lon_max = geo.Coord.new();
	median_lon_max.set_latlon(median_lat, max_lon, geo.elevation(median_lat, max_lon, 10000));
	var xdist = distance_lon_m(min_coord, median_lon_max);
	
	# get latitudal distance in meters across median
	var median_lat_max = geo.Coord.new();
	median_lat_max.set_latlon(max_lat, median_lon, geo.elevation(max_lat, median_lon, 10000));
	var ydist = distance_lat_m(min_coord, median_lat_max);

	var xsamples = xdist / resolution;
	if ((xsamples - int(xsamples)) > 0.00001) {
		# extra sample to cover remainder
		xsamples = int(xsamples) + 2;
	}
	else {
		xsamples = int(xsamples) + 1;
	}

	var ysamples = ydist / resolution;
	if ((ysamples - int(ysamples)) > 0.00001) {
	    # extra sample to cover remainder
		ysamples = int(ysamples) + 2;
	}
	else {
		ysamples = int(ysamples) + 1;
	}

	var fullpath = getprop("/sim/fg-home") ~ "/Export/" ~ filename ~ ".fgts";
	if (io.stat(fullpath) != nil and !overwrite) {
		setprop("/sim/messages/copilot", fullpath ~ " already exists and overwrite not requested..bailing out.");
		return;
	}

	var fout = io.open(fullpath, "w");
	
	# header attributes
	fprintf(fout, "%s\n", "ATTRIBUTES {");
	fprintf(fout, "min_lat=%.5f\n", min_lat);
	fprintf(fout, "min_lon=%.5f\n", min_lon);
	fprintf(fout, "max_lat=%.5f\n", max_lat);
	fprintf(fout, "max_lon=%.5f\n", max_lat);
	fprintf(fout, "median_lat=%.5f\n", median_lat);
	fprintf(fout, "median_lon=%.5f\n", median_lon);
	fprintf(fout, "median_alt=%.5f\n", median_alt);
	fprintf(fout, "span_lat_m=%.5f\n", distance_lat_m(min_coord, max_coord));
	fprintf(fout, "span_lon_m=%.5f\n", distance_lon_m(min_coord, max_coord));
	fprintf(fout, "xsamples=%d\n", xsamples);
	fprintf(fout, "ysamples=%d\n", ysamples);
	fprintf(fout, "%s\n", "}");
	#
	
	var coord = geo.Coord.new();

	for (var y = 0; y < ysamples; y += 1) {
		for (var x = 0; x < xsamples; x += 1) {
			coord.set(min_coord);

			if (y) {
				if (y == ysamples - 1)
					coord.set_lat(max_lat); # last sample this col, clamp to max
				else
					coord.apply_course_distance(NORTH, y * resolution);
			}

			if (x) {
				if (x == xsamples - 1)				
					coord.set_lon(max_lon); # last sample this row, clamp to max
				else
					coord.apply_course_distance(EAST, x * resolution);
			}
			
			var info = geodinfo(coord.lat(), coord.lon(), 10000);
			var alt_m = info == nil ? -32767 : info[0];
			var lat_m = distance_lat_m(min_coord, coord);
			var lon_m = distance_lon_m(min_coord, coord);

			fprintf(fout, "%s%.5f,%.5f,%.5f", x > 0 ? "," : "", lat_m, lon_m, alt_m);
		}
		io.write(fout, "\n");
	}
	io.close(fout);
}
