#include <simgear/bucket/newbucket.hxx>

#include <cstdio>
#include <sstream>

int main(int argc, char** argv)
{
	if (argc < 2) {
		fprintf(stderr, "usage: %s bucket_id\n",  argv[0]);
		return 1;
	}

	long int bucket_id;
	std::istringstream is(argv[1]);
	is >> bucket_id;
		
	SGBucket b(bucket_id);
	std::ostringstream os;
	os << b;

	float lat, lon;
	int x, y;
	sscanf(os.str().c_str(), "%f:%d, %f:%d", &lon, &x, &lat, &y);

	fprintf(stdout, "%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%d,%d\n", lat, lon, b.get_center_lat(),
			b.get_center_lon(), b.get_width(), b.get_height(), b.get_x(), b.get_y());

	return 0;
}
